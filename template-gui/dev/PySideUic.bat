<# : batch
@echo off
setlocal
cd %~dp0
powershell -ExecutionPolicy RemoteSigned -Command "iex $(gc '%~f0' -Raw)"
endlocal
goto:eof
#>

. ".\_Common.ps1"

Set-Location ".."

. ".venv\Scripts\Activate.ps1"
pyside6-uic "-o" "src\ui\main_window.py" "ui\main_window.ui"
$ErrorCode = $LastExitCode
deactivate
if ($ErrorCode -ne 0) {
    Stop-Shell "Failed to make UI code by error: $ErrorCode" 1
}

Stop-Shell "Make UI code was done" 0
