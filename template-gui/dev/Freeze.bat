<# : batch
@echo off
setlocal
cd %~dp0
powershell -ExecutionPolicy RemoteSigned -Command "iex $(gc '%~f0' -Raw)"
endlocal
goto:eof
#>

. ".\_Common.ps1"

Set-Location ".."

# Configuration
$entryName = "main"
$iconPath = "res\icon.ico"

# Make options for pyinstaller
$options = @();
$options += ".\src\$entryName.py"
$options += "--clean"
$yesno = Read-Host "Do you want to create a one-file bundled executable? [Y/n]"
$onefile = $false
switch ($yesno) {
    "n" { } # a one-folder bundle containing an executable
    default {
        $onefile = $true
        $options += "-F"
    }
}
$options += "-p"
$options += "./src"
$options += "-w"
if (Test-Path ".\$iconPath") {
    $options += "-i"
    $options += "$iconPath"
}

# Make executable binary to `.\dist`
Write-Host "Freezing python application..."
. ".venv\Scripts\Activate.ps1"
pyinstaller $options
$errorCode = $LastExitCode
deactivate
if ($errorCode -ne 0) {
    Stop-Shell "Failed to freeze python application by error: $errorCode" 1
}

# Get name and version of the application
$pyproject = Get-Content "pyproject.toml"
$appName = ""
foreach ($line in $pyproject) {
    if ($line -match '^name = "(?<AppName>[^"]+?)"$') {
        $appName = $Matches.AppName
        break
    }
}
if ($appName -eq "") {
    Stop-Shell "Failed to freeze python application: AppName is not found in the pyproject.toml" 1
}
$version = ""
foreach ($line in $pyproject) {
    if ($line -match '^version = "(?<Version>[^"]+?)"$') {
        $version = $Matches.Version
        break
    }
}
if ($version -eq "") {
    Stop-Shell "Failed to freeze python application: Version is not found in the pyproject.toml" 1
}

# Move executable binary to `.\bin`
Remove-Dir ".\bin"
$appdir = ".\bin\$appName"
if ($onefile) {
    New-Item $appdir -ItemType Directory
    Move-Item ".\dist\$entryName.exe" "$appdir\$appName.exe"
}
else {
    New-Item ".\bin" -ItemType Directory
    Move-Item ".\dist\$entryName" "$appdir"
    Move-Item "$appdir\$entryName.exe" "$appdir\$appName.exe"
}

# Make compressed archive
Compress-Archive -Path $appdir -DestinationPath "${appdir}_v$version.zip"

# Remove temporary directories
Remove-Dir ".\build" 
Remove-Dir ".\dist"
Remove-Item "*.spec"

Stop-Shell "Freeze was done" 0
