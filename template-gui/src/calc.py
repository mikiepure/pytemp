def add(a: int, b: int) -> int:
    """Calculate add operation. Return value of a + b.

    Args:
        a (int): Left value.
        b (int): Right value.

    Returns:
        int: Added value.
    """
    return a + b
