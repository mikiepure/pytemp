import sys

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication, QMainWindow

from calc import add
from ui.main_window import Ui_MainWindow
from util.exc import except_hook_logging
from util.log import LOG
from util.path import ICON_DIR


class MainWindow(QMainWindow):
    def __init__(self, parent=None) -> None:
        super(MainWindow, self).__init__()
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)
        self.setWindowIcon(QIcon(str(ICON_DIR / "icon.ico")))

        self._ui.pushButtonCalc.clicked.connect(self._calc_clicked)

    def _calc_clicked(self) -> None:
        res = add(
            int(self._ui.lineEditValueA.text()), int(self._ui.lineEditValueB.text())
        )
        self._ui.lineEditResult.setText(str(res))


def main() -> None:
    """Entry point of the application."""
    LOG.info("START")
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    sys.excepthook = except_hook_logging
    main()
