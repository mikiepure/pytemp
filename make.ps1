function Stop-Shell ([string]$msg, [int]$code) {
    if ($code -eq 0) {
        Write-Host $msg
    }
    else {
        Write-Host $msg -ForegroundColor Red
    }
    Write-Host -NoNewLine "Press any key to exit..."
    $null = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    exit $code
}

# Check required command
Get-Command py *> $null
if (!$?) {
    Stop-Shell "'py' is required to use the script; Please install it" 1
}
Get-Command poetry *> $null
if (!$?) {
    Stop-Shell "'poetry' is required to use the script; Please install it" 1
}

# Input required information
$projectName = Read-Host("Project Name")
$projectType = Read-Host("Project Type (cui or gui)")
if ("cui", "gui" -notcontains $projectType) {
    Stop-Shell "ERR: Input string is not 'cui' nor 'gui': $projectType" 1
}

# Input optional information
$pyArgs = @()
if ((py --list | Measure-Object -Line | Select-Object -ExpandProperty Lines) -ne 1) {
    Write-Host "The following pythons are available:"
    py --list | Select-String " -V:(\d\.\d+(-32)?)" | ForEach-Object { $_.Matches.Groups[1].Value }
    $pyVerDefualt = py --list | Select-String " -V:(\d\.\d+(-32)?) \*" | ForEach-Object { $_.Matches.Groups[1].Value }
    $pyVer = Read-Host("Select python version [$pyVerDefualt]")
    if ($pyVer -ne "") {
        $pyVerCount = py --list | Select-String " -V:$pyVer" -SimpleMatch | Measure-Object -Line | Select-Object -ExpandProperty Lines
        if ($pyVerCount -eq 0) {
            Stop-Shell "ERR: Python Version is not exists: $pyVer" 1
        }

        $pyArgs += "-$pyVer"
    }
}

# Check/Get required files
$requiredFilesOnLocal = (Test-Path ".\template" -PathType Container) -And (Test-Path ".\template-${projectType}" -PathType Container)
if (!($requiredFilesOnLocal)) {
    Write-Host "Get required files from web..."
    Invoke-WebRequest https://gitlab.com/mikiepure/pytemp/-/archive/master/pytemp-master.zip -OutFile "pytemp-master.zip"
    if (!($?)) {
        Stop-Shell "Failed to get pytemp-master from web by error" 1
    }
    Expand-Archive "pytemp-master.zip" . -Force
    Remove-Item ".\pytemp-master.zip" -Force
    Copy-Item ".\pytemp-master\template" . -Recurse -Force
    Copy-Item ".\pytemp-master\template-${projectType}" . -Recurse -Force
    Remove-Item ".\pytemp-master" -Recurse -Force
}

Write-Host "Making poetry project..."
poetry new $projectName --src -q
Push-Location $projectName

Write-Host "Rewrite python dependency version..."
$pythonVersion = 'python = ">=3.11,<3.13"  # "<3.13" is required by pyinstaller'
(Get-Content "pyproject.toml") | Where-Object { $_ -notlike 'packages = `[*`]' } | Set-Content "pyproject.toml"
(Get-Content "pyproject.toml") | ForEach-Object { $_ -replace '^python = ".+"$', "$pythonVersion" } | Set-Content "pyproject.toml"

Write-Host "Creating python virtual environment..."
py @($pyArgs) -m venv ".venv"
if ($LastExitCode -ne 0) {
    Stop-Shell "Failed to create python virtual environment by error: $LastExitCode" 1
}

Write-Host "Installing dependency packages..."
poetry add ruff pytest pyinstaller -G dev
if ($LastExitCode -ne 0) {
    Stop-Shell "Failed to install dependency packages by error: $LastExitCode" 1
}
if ($projectType -eq "gui") {
    poetry add pyside6
    if ($LastExitCode -ne 0) {
        Stop-Shell "Failed to install dependency packages for gui by error: $LastExitCode" 1
    }
}

Write-Host "Append tool settings to toml file..."
Add-Content "pyproject.toml" @'


[tool.ruff]
exclude = [
    ".git",
    ".pytest_cache",
    ".ruff_cache",
    ".venv",
    "__pycache__",
    "src/ui",
]
src = ["src"]

[tool.pytest.ini_options]
pythonpath = "src"
'@

Write-Host "Override files from templates..."
Remove-Item "src" -Recurse -Force
Remove-Item "tests" -Recurse -Force
Pop-Location
Copy-Item ".\template\*" ".\$projectName" -Force -Recurse
Copy-Item ".\template-${projectType}\*" ".\$projectName" -Force -Recurse

if (!($requiredFilesOnLocal)) {
    Write-Host "Remove required files from web..."
    Remove-Item ".\template" -Recurse -Force
    Remove-Item ".\template-${projectType}" -Recurse -Force
}

Write-Host "Setup was done"
