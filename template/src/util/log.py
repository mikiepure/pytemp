"""Provide logger of the application."""
import logging
from datetime import datetime
from logging import FileHandler, Formatter, Logger, StreamHandler, getLogger
from typing import Optional

from util.path import LOG_DIR

###############################################################################
# Public
###############################################################################


def get_logger(
    name: str,
    level: str = "DEBUG",
    to_stream_level: Optional[str] = "INFO",
    to_file_level: Optional[str] = None,
) -> Logger:
    """Get logger.

    Get logger which can output log to stdout and file.
    The following space separated format is used for the log:
    - 2000/01/01 00:00:00.000 INFO. message (file.py:0)
      ^^^^^^^^^^ ^^^^^^^^^^^^ ^^^^^ ^^^^^^^  ^^^^    ^ lineno
      date       time         level message  filename

    About the file:
    - The filename is "date-time.txt" when the output is started.
    - The file is stored in "log" directory.

    Args:
        name (str): A name of the logger.
        level (str, optional): Log level to the logger. Defaults to "DEBUG".
        to_stream_level (Optional[str], optional): Log level to output stream.
                                                   Do not output if it is None.
                                                   Defaults to "INFO".
        to_file_level (Optional[str], optional): Log level to output file.
                                                 Do not output if it is None.
                                                 Defaults to None.

    Returns:
        Logger: Created logger.
    """
    # setup logger
    logger = getLogger(name)
    logger.setLevel(level)

    formatter = Formatter(
        "%(asctime)s.%(msecs)03d %(levelname)s %(message)s "
        "(%(filename)s:%(lineno)d)",
        "%Y/%m/%d %H:%M:%S",
    )

    # setup stream handler
    if to_stream_level is not None:
        stream_handler = StreamHandler()
        stream_handler.setLevel(to_stream_level)
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    # setup file handler
    if to_file_level is not None:
        now = datetime.now().strftime("%Y%m%d-%H%M%S")
        path = LOG_DIR / (now + ".txt")
        LOG_DIR.mkdir(exist_ok=True)
        file_handler = FileHandler(str(path))
        file_handler.setLevel(to_file_level)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    return logger


# global logger for the application
LOG = get_logger("GLOBAL")


###############################################################################
# Private
###############################################################################

# set the name of the log level in 5 charactors
logging.addLevelName(0, "NOSET")
logging.addLevelName(10, "DEBUG")
logging.addLevelName(20, "INFO.")
logging.addLevelName(30, "WARN.")
logging.addLevelName(40, "ERROR")
logging.addLevelName(50, "FATAL")
