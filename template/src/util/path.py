""" Provide path information of the application. """
import sys
from pathlib import Path

###############################################################################
# Public
###############################################################################

APP_DIR = (
    Path(sys.executable).parent.resolve()
    if getattr(sys, "frozen", False)
    else Path(__file__).parent.parent.parent.resolve()
)
LOG_DIR = APP_DIR / "log"
ICON_DIR = Path(
    getattr(
        sys, "_MEIPASS", str((Path(__file__).parent.parent.parent / "res").resolve())
    )
)
