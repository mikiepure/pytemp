"""Provite method to handle exception."""
import sys
import traceback
from types import TracebackType
from typing import Type

from util.log import LOG


def except_hook_logging(
    t: Type[BaseException], v: BaseException, tb: TracebackType
) -> None:
    """Logging exception.

    Args:
        t (Type[BaseException]): Type of the exception
        v (BaseException): Object of the exception
        tb (TracebackType): Traceback of the exception
    """
    if issubclass(t, Exception):
        LOG.fatal("=" * 80)
        LOG.fatal("Uncaught Exception: %s", v)
        LOG.fatal("-" * 80)
        for s in "".join(traceback.format_exception(t, v, tb)).strip().split("\n"):
            LOG.fatal("%s", s)
        LOG.fatal("=" * 80)
    sys.__excepthook__(t, v, tb)
