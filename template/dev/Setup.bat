<# : batch
@echo off
setlocal
cd %~dp0
powershell -ExecutionPolicy RemoteSigned -Command "iex $(gc '%~f0' -Raw)"
endlocal
goto:eof
#>

. ".\_Common.ps1"

Set-Location ".."

Get-Command py *> $null
if (!$?) {
    Stop-Shell "'py' is required to use the script; Please install it" 1
}
Get-Command poetry *> $null
if (!$?) {
    Stop-Shell "'poetry' is required to use the script; Please install it" 1
}

if (Test-Path ".venv") {
    Write-Host "Python virtual environment is already created"
    $yesno = Read-Host "Do you want to re-create it? [y/N]"
    switch ($yesno) {
        "y" {
            Write-Host "Removing python virtual environment..."
            Remove-Dir ".venv"
            if (Test-Path ".venv") {
                Stop-Shell "Failed to remove python virtual environment" 1
            }
        }
        default {
            exit 0
        }
    }
}

$pyArgs = @()
if ((py --list | Measure-Object -Line | Select-Object -ExpandProperty Lines) -ne 1) {
    Write-Host "The following pythons are available:"
    py --list | Select-String " -V:(\d\.\d+(-32)?)" | ForEach-Object { $_.Matches.Groups[1].Value }
    $pyVerDefualt = py --list | Select-String " -V:(\d\.\d+(-32)?) \*" | ForEach-Object { $_.Matches.Groups[1].Value }
    $pyVer = Read-Host("Select python version [$pyVerDefualt]")
    if ($pyVer -ne "") {
        $PyVerCount = py --list | Select-String " -V:$pyVer" -SimpleMatch | Measure-Object -Line | Select-Object -ExpandProperty Lines
        if ($PyVerCount -eq 0) {
            Stop-Shell "ERR: Python Version is not exists: $pyVer" 1
        }
        
        $pyArgs += "-$pyVer"
    }
}

Write-Host "Creating python virtual environment..."
py @($pyArgs) -m venv ".venv"
if ($LastExitCode -ne 0) {
    Stop-Shell "Failed to create python virtual environment by error: $LastExitCode" 1
}

Write-Host "Installing python required packages..."
poetry install
if ($LastExitCode -ne 0) {
    Stop-Shell "Failed to install python required packages by error: $LastExitCode" 1
}

Stop-Shell "Setup was done" 0
