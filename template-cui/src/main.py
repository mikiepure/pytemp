import sys

from calc import add
from util.exc import except_hook_logging
from util.log import LOG


def main() -> None:
    """Entry point of the application."""
    LOG.info("START")
    print("Hello World!")
    print("1 + 2 = " + str(add(1, 2)))
    LOG.info("-END-")


if __name__ == "__main__":
    sys.excepthook = except_hook_logging
    main()
